#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os, html5lib
import xml.etree.ElementTree as ET
import json, pprint
from argparse import ArgumentParser

p = ArgumentParser()
p.add_argument("--mode", default="remote")
args = p.parse_args()

def get_toc(htmlsrc, title):
    tree = html5lib.parse(htmlsrc, namespaceHTMLElements=False)
    tocorder = []
    chaptersNarticles = tree.findall('.//body/ul/li')

    for li in chaptersNarticles:        
	ch= li.findall('.//b')[0]
	print 'chapter', ch.tag, ch.text
        tocorder.append( ('Chapter', '',  ch.text) )
        articles = li.findall('.//ul/li')
        for children in articles:

            anchors = children.findall('.//a')
            article_link = anchors[0].get('title') #link is create from title
            article_title = anchors[0].text
            print 'article', article_link, article_link
            tocorder.append( ('Article', article_link, article_title) )
            translations = [a for a in anchors if len(a.text)==2 ] # eg NL, EN, FR

            if len(translations) > 0:
                print 'translations', len(translations)

                for trans in translations:
                    article_title = trans.get('title')
                    article_link = article_title
                    trans.set('href', article_link)
                    tocorder.append( ('Article', article_link, article_title) )
                    print 'APPEND Article', article_link

                    # FR Amateur librarian is not in
                    # these texts are not it text-html/
                    # it might have with t


    tocorder.insert(1, ('Article', u'The radiated book', u'Index'))
    return tocorder


def manipulate_indexpage(htmlsrc, title):
    # link from index to articles, comes from <a title=XYZ>
    # where XYZ comes frome the wiki's PAGE NAME
    
    tree = html5lib.parse(htmlsrc, namespaceHTMLElements=False)    
    toc=tree.find('.//div[@id="toc"]')
    tocanchors  = tree.findall('.//li/i/a')
    for anchor in tocanchors:
        anchor.set('href', "#"+ (anchor.get('title').replace('/','_') ))

        if " (print)" in anchor.get('href'):
            href = (anchor.get('href')).replace(" (print)", "")
            title = anchor.get('title').replace(" (print)", "")
            anchor.set('href',href )
            anchor.set('title',title )
        elif "LES UTOPISTES" in anchor.text:
            anchor.set('href',"#les utopistes" )
            
        # also: div.article#colophon/colofon -> div.article#colophon\.colofon
    return tree

##########################
## Manipulate articles html defs ##
#########################
def manipulate_imgs(tree):
    # unwrap imgs from anchor tags, replacing by div
    # reason: anchor tags surrounding imgs prevent css rule: page-break-during:avoid 
    img_anchors = tree.findall('.//a[@class="image"]')
    for anchor in img_anchors:
        anchor.tag = 'div' #change a.image to div.image
    # set imgs href to full url
    imgs = tree.findall('.//img') 
    for img in imgs: 
        src = img.get('src')     

        if args.mode == 'remote':
            newsrc = "http://www.mondotheque.be"+img.get('src') ## CHECK if not breaking other imgs
        elif args.mode == 'local':
            newsrc = "texts-html/imgs/"+ ((img.get('src').split('/'))[-1])

        img.set('src', newsrc) #set img src to mondotheque url 
        if 'srcset' in img.attrib: # rm srcset 
            img.set('srcset', '')



def manipulate_footnotes(tree, title):
    title = title.replace(" ", "_")
    allreferences = tree.findall('.//ol[@class="references"]/li')
    allfootnotes =  tree.findall('.//sup[@class="reference"]')
    for sup in allfootnotes:
        new_id = sup.get('id')+"-"+title
        sup.set('id', new_id)
        a = sup.find('.//a')
        new_href = a.get('href')+"-"+title
        a.set('href', new_href)
    for li in allreferences:
        new_id = li.get('id')+"-"+title
        li.set('id', new_id)
        a = (li.find('.//a'))
        new_href = a.get('href')+"-"+title
        a.set('href', new_href)


def remove_heading_span(h_el): 
# in MW headings normaly contain a <span> that holds the title of the heading
# here we remove that span and put the title in the heading .text
    children = h_el.findall('.//')
    if len(children) is 1 and children[0].tag == 'span':    
        span = children[0]
        title =  span.text
        el_id = span.get('id')
        h_el.remove( span )
        h_el.text = title
        h_el.set('id', el_id)
        
    if len(children) > 1 and children[0].tag == 'span':  #if more than one children
        # as in case of Madame C
        # remove span
        # and make its child, usualy <a> a direct child of the heading
        span = children[0]
        el_id = span.get('id')
        grandchildren = children
        grandchildren.remove(span)
        h_el.remove( span )
        h_el.set('id', el_id)
        for el in grandchildren:
            h_el.append(el)

        
def manipulate_hierarchy(tree): 
# ensures no heading within an article is > h3. 
# h1 if for chapter titles. h2 for article titles.
    headings = [el for el in tree.findall('.//') if el.tag in ["h1","h2"]]
    for h in headings:
        remove_heading_span(h)
    headingtags = [el.tag for el in headings]

    if 'h1' in headingtags: #how much to add to heading
        add = 2
    elif 'h2' in headingtags:
        add = 1
    else:
        add = 0        

    if add != 0:
        for h in headings:
            newtag =  h.tag[0]+ str(int(h.tag[-1]) + add)
            h.tag=newtag 

     
    
def manipulate_html(htmlsrc, title):
    tree = html5lib.parse(htmlsrc, namespaceHTMLElements=False)    
    manipulate_imgs(tree)
    manipulate_footnotes(tree, title)
    manipulate_hierarchy(tree)    
    body=tree.find('./body')     
    children = body.getchildren()
    articlediv = ET.Element('div')# div to wrap articles
    articlediv.set('class', 'article')
    if "LES UTOPISTES" in title:
        articlediv.set('id', 'les utopistes' )
        print ' ----> LES UTOPISTES:', title 
    else:
        articlediv.set('id', title.replace("/","_"))

    articletitle = ET.Element('h2')# articles title
#    print '    **', articletitle, title.replace("/","_") 

    articletitle.set('class', 'title')
    title = title
    articletitle.text = title
    articlediv.append(articletitle)

    for el in children:
        tag = el.tag
        if not hasattr(tag, '__call__'): #if not a Comment: 
            articlediv.append(el)
    return articlediv


def addchapter(chaptitle):
    h1 = ET.Element('h1')
    h1.text = chaptitle
    h1.set('class', 'chaptertitle')        
    return h1


html_dir="texts-html"

def workonfile(pagename, articletitle):
    htmlfile = html_dir + '/'+(pagename.replace(" ","_")) + ".html"
    if os.path.isfile(htmlfile): #only existing files
        with open(htmlfile, 'rb') as f:             
            if articletitle == 'Index': 
                f = ET.tostring (manipulate_indexpage(f, articletitle))
            articlediv = manipulate_html(f, articletitle)
            return articlediv
    else:
        print '    NOT FOUND', htmlfile
        
def write_html_file(html_tree, filename):
    htmlcontent = ET.tostring(html_tree,  method='html', encoding='utf-8') 
    template= file('resources/book-template.html', 'r')
    template= template.read()
    htmlwhole = template.format( htmlcontent )
    edited = open(filename, 'w') #write
    edited.write(htmlwhole)
    edited.close()


bookdiv = ET.Element('div') # parent div to wrap all articles 
bookdiv.set('class', 'bookwrap')

with open(html_dir+'/'+'The_radiated_book.html', 'rb') as f: # toc order          
    toc = get_toc(f,'The radiated book')
    print 'TOC', toc, '(Level, Link, Title)' # TOC chapter eg ('Chapter, '', 'Disambiguation')

for entry in toc:
    if entry[0] is 'Chapter': # insert chapter or article's title
        print 'Chapter', entry[2] 
        chaptertitle = addchapter(entry[2])
        bookdiv.append(chaptertitle)
    elif entry[0] is 'Article':
        print 'filename:', entry[1].replace('/','_'), 'title:', entry[2] 
        articlediv = workonfile(entry[1].replace('/','_'), entry[2])         
        if articlediv is not None:
            bookdiv.append(articlediv)
                



            
write_html_file(bookdiv, "book.html" )





 
